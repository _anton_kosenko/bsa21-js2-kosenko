const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Weather {
    /**
     * Describe attributes: temperature, wind, forecast, date
     */
    constructor() {
        this.temperature = ''
        this.wind = ''
        this.forecast = [{ day: '' }, { temperature: '' }, { wind: '' }]
        this.date = Date.now()
    }

    //setForecast() doesn't work ???
    async setForecast(cityName) {
        await axios.get(weatherUrl + cityName).then((response) => {
            this.forecast.day = response.forecast.data.day
            this.forecast.temperature = response.forecast.data.temperature
            this.forecast.wind = response.forecast.data.wind
        }).catch((error) => { return error.message })
    }

    async setWeather(cityName) {
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature
            this.wind = response.data.wind
            this.forecast = setForecast(cityName)
        }).catch((error) => { return error.message })
    }
}

module.exports = { Weather }