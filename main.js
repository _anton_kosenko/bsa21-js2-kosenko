const { Country } = require('./Country')
const { City } = require('./City')

// addCity function doesn't work, all cities add to the country.cities with push() method 
let country1 = new Country('Ukraine')
console.log(country1)
let city1 = new City('Kharkiv')
country1.cities.push(city1)
console.log(country1)
let city2 = new City('Lviv')
// country1.addCity(city2)
// console.log(country1)
country1.cities.push(city2)
// console.log(country1)
let city3 = new City('Kyiv')
country1.cities.push(city3)
let city4 = new City('Odesa')
country1.cities.push(city4)
let city5 = new City('Mykolaiv')
country1.cities.push(city5)
let city6 = new City('Cherkasy')
country1.cities.push(city6)
let city7 = new City('Dnipro')
country1.cities.push(city7)
let city8 = new City('Zhytomyr')
country1.cities.push(city8)
let city9 = new City('Ternopil')
country1.cities.push(city9)
let city10 = new City('Poltava')
country1.cities.push(city10)

// setForecast() doesn't work so I comment them!!! 
city1.setWeather().then(() => { console.log(city1) }).catch((error) => { console.log(error) })
// city1.setForecast().then(() => { console.log(city1) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city1)
//console.log(country1.cities[0])

city2.setWeather().then(() => { console.log(city2) }).catch((error) => { console.log(error) })
// city2.setForecast().then(() => { console.log(city2) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city2)

city3.setWeather().then(() => { console.log(city3) }).catch((error) => { console.log(error) })
// city3.setForecast().then(() => { console.log(city3) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city3)

city4.setWeather().then(() => { console.log(city4) }).catch((error) => { console.log(error) })
// city4.setForecast().then(() => { console.log(city4) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city4)

city5.setWeather().then(() => { console.log(city5) }).catch((error) => { console.log(error) })
// city5.setForecast().then(() => { console.log(city5) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city5)

city6.setWeather().then(() => { console.log(city6) }).catch((error) => { console.log(error) })
// city6.setForecast().then(() => { console.log(city6) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city6)

city7.setWeather().then(() => { console.log(city7) }).catch((error) => { console.log(error) })
// city7.setForecast().then(() => { console.log(city7) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city7)

city8.setWeather().then(() => { console.log(city8) }).catch((error) => { console.log(error) })
// city8.setForecast().then(() => { console.log(city8) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city8)

city9.setWeather().then(() => { console.log(city9) }).catch((error) => { console.log(error) })
// city9.setForecast().then(() => { console.log(city9) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city9)

city10.setWeather().then(() => { console.log(city10) }).catch((error) => { console.log(error) })
// city10.setForecast().then(() => { console.log(city10) }).catch((error) => { console.log(error) })
// country1.cities = country1.addCity(city10)

country1.sortCities(country1.cities)
console.log(country1)

country1.dellCity("Kharkiv")
console.log(country1)
