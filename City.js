const { Weather } = require('./Weather')

class City {
    /**
     * Describe attributes: name, weather
     */
    constructor(name) {
        this.name = name
        this.weather = new Weather()
    }

    async setWeather() {
        await this.weather.setWeather(this.name)
    }

    // setForecast() doesn't work ???
    async setForecast() {
        await this.weather.forecast.setForecast(this.name)
    }
}

module.exports = { City }