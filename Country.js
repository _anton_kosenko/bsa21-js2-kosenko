const { City } = require('./City')

class Country {
   /**
    * Describe attributes: name, cities array
    */
   constructor(name, cities) {
      this.name = name
      this.cities = []
   }

   // this function doesn't work
   addCity(city) {
      let newCities = this.cities.push(city)
      this.cities = newCities
      return this.cities
   }

   dellCity(city) {
      let index = this.cities.findIndex((item) => item.name === city)
      this.cities.splice(index, 1)

      // let index = cities.indexOf(city)
      // if (index > -1) {
      //    cities.splice(index, 1)
      // }
      return this.cities
   }

   sortCities(cities) {
      cities.sort(function (a, b) {
         for (let i = 0; i < cities.length; i++) {
            if (cities[i].temperature > cities[i + 1].temperature) {
               return 1;
            }
            if (cities[i].temperature < cities[i + 1].temperature) {
               return -1
            }
            return 0
         }
      })

      return cities
   }

}

module.exports = { Country }